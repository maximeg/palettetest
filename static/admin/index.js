
const homeSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('path', {
    d: 'M19 9L19 17C19 18.8856 19 19.8284 18.4142 20.4142C17.8284 21 16.8856 21 15 21L14 21L10 21L9 21C7.11438 21 6.17157 21 5.58579 20.4142C5 19.8284 5 18.8856 5 17L5 9',
    stroke: '#FFFFFF',
    'stroke-width': '1.5',
    'stroke-linejoin': 'round'
  }),
  h('path', {
    d: 'M3 11L7.5 7L10.6713 4.18109C11.429 3.50752 12.571 3.50752 13.3287 4.18109L16.5 7L21 11',
    stroke: '#FFFFFF',
    'stroke-width': '1.5',
    'stroke-linecap': 'round',
    'stroke-linejoin': 'round'
  }),
  h('path', {
    d: 'M10 21V17C10 15.8954 10.8954 15 12 15V15C13.1046 15 14 15.8954 14 17V21',
    stroke: '#FFFFFF',
    'stroke-width': '1.5',
    'stroke-linecap': 'round',
    'stroke-linejoin': 'round'
  })
]);

const menuSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('path', {
    opacity: '0.4',
    d: 'M16.19 2H7.81C4.17 2 2 4.17 2 7.81V16.18C2 19.83 4.17 22 7.81 22H16.18C19.82 22 21.99 19.83 21.99 16.19V7.81C22 4.17 19.83 2 16.19 2Z',
    fill: '#FFFFFF'
  }),
  h('path', {
    d: 'M17 8.25H7C6.59 8.25 6.25 7.91 6.25 7.5C6.25 7.09 6.59 6.75 7 6.75H17C17.41 6.75 17.75 7.09 17.75 7.5C17.75 7.91 17.41 8.25 17 8.25Z',
    fill: '#FFFFFF'
  }),
  h('path', {
    d: 'M17 12.75H7C6.59 12.75 6.25 12.41 6.25 12C6.25 11.59 6.59 11.25 7 11.25H17C17.41 11.25 17.75 11.59 17.75 12C17.75 12.41 17.41 12.75 17 12.75Z',
    fill: '#FFFFFF'
  }),
  h('path', {
    d: 'M17 17.25H7C6.59 17.25 6.25 16.91 6.25 16.5C6.25 16.09 6.59 15.75 7 15.75H17C17.41 15.75 17.75 16.09 17.75 16.5C17.75 16.91 17.41 17.25 17 17.25Z',
    fill: '#FFFFFF'
  })
]);

const settingsSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('circle', {
    cx: '12',
    cy: '12',
    r: '3',
    stroke: '#FFFFFF',
    'stroke-width': '1.5'
  }),
  h('path', {
    d: 'M13.7654 2.15224C13.3978 2 12.9319 2 12 2C11.0681 2 10.6022 2 10.2346 2.15224C9.74457 2.35523 9.35522 2.74458 9.15223 3.23463C9.05957 3.45834 9.0233 3.7185 9.00911 4.09799C8.98826 4.65568 8.70226 5.17189 8.21894 5.45093C7.73564 5.72996 7.14559 5.71954 6.65219 5.45876C6.31645 5.2813 6.07301 5.18262 5.83294 5.15102C5.30704 5.08178 4.77518 5.22429 4.35436 5.5472C4.03874 5.78938 3.80577 6.1929 3.33983 6.99993C2.87389 7.80697 2.64092 8.21048 2.58899 8.60491C2.51976 9.1308 2.66227 9.66266 2.98518 10.0835C3.13256 10.2756 3.3397 10.437 3.66119 10.639C4.1338 10.936 4.43789 11.4419 4.43786 12C4.43783 12.5581 4.13375 13.0639 3.66118 13.3608C3.33965 13.5629 3.13248 13.7244 2.98508 13.9165C2.66217 14.3373 2.51966 14.8691 2.5889 15.395C2.64082 15.7894 2.87379 16.193 3.33973 17C3.80568 17.807 4.03865 18.2106 4.35426 18.4527C4.77508 18.7756 5.30694 18.9181 5.83284 18.8489C6.07289 18.8173 6.31632 18.7186 6.65204 18.5412C7.14547 18.2804 7.73556 18.27 8.2189 18.549C8.70224 18.8281 8.98826 19.3443 9.00911 19.9021C9.02331 20.2815 9.05957 20.5417 9.15223 20.7654C9.35522 21.2554 9.74457 21.6448 10.2346 21.8478C10.6022 22 11.0681 22 12 22C12.9319 22 13.3978 22 13.7654 21.8478C14.2554 21.6448 14.6448 21.2554 14.8477 20.7654C14.9404 20.5417 14.9767 20.2815 14.9909 19.902C15.0117 19.3443 15.2977 18.8281 15.781 18.549C16.2643 18.2699 16.8544 18.2804 17.3479 18.5412C17.6836 18.7186 17.927 18.8172 18.167 18.8488C18.6929 18.9181 19.2248 18.7756 19.6456 18.4527C19.9612 18.2105 20.1942 17.807 20.6601 16.9999C21.1261 16.1929 21.3591 15.7894 21.411 15.395C21.4802 14.8691 21.3377 14.3372 21.0148 13.9164C20.8674 13.7243 20.6602 13.5628 20.3387 13.3608C19.8662 13.0639 19.5621 12.558 19.5621 11.9999C19.5621 11.4418 19.8662 10.9361 20.3387 10.6392C20.6603 10.4371 20.8675 10.2757 21.0149 10.0835C21.3378 9.66273 21.4803 9.13087 21.4111 8.60497C21.3592 8.21055 21.1262 7.80703 20.6602 7C20.1943 6.19297 19.9613 5.78945 19.6457 5.54727C19.2249 5.22436 18.693 5.08185 18.1671 5.15109C17.9271 5.18269 17.6837 5.28136 17.3479 5.4588C16.8545 5.71959 16.2644 5.73002 15.7811 5.45096C15.2977 5.17191 15.0117 4.65566 14.9909 4.09794C14.9767 3.71848 14.9404 3.45833 14.8477 3.23463C14.6448 2.74458 14.2554 2.35523 13.7654 2.15224Z',
    stroke: '#FFFFFF',
    'stroke-width': '1.5'
  })
]);

const postSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('path', {
    d: 'M2 5C2 3.34315 3.34315 2 5 2H19C20.6569 2 22 3.34315 22 5V19C22 20.6569 20.6569 22 19 22H5C3.34315 22 2 20.6569 2 19V5ZM5 4C4.44772 4 4 4.44772 4 5V10H20V5C20 4.44772 19.5523 4 19 4H5ZM4 12V19C4 19.5523 4.44772 20 5 20H19C19.5523 20 20 19.5523 20 19V12H4ZM14 13C14.2652 13 14.5196 13.1054 14.7071 13.2929L18.7071 17.2929C19.0976 17.6834 19.0976 18.3166 18.7071 18.7071C18.3166 19.0976 17.6834 19.0976 17.2929 18.7071L14 15.4142L11.7071 17.7071L10.7071 18.7071C10.3166 19.0976 9.68342 19.0976 9.29289 18.7071C8.90237 18.3166 8.90237 17.6834 9.29289 17.2929L9.58579 17L9 16.4142L6.70711 18.7071C6.31658 19.0976 5.68342 19.0976 5.29289 18.7071C4.90237 18.3166 4.90237 17.6834 5.29289 17.2929L8.29289 14.2929C8.48043 14.1054 8.73478 14 9 14C9.26522 14 9.51957 14.1054 9.70711 14.2929L11 15.5858L13.2929 13.2929C13.4804 13.1054 13.7348 13 14 13ZM11 7C11 6.44772 11.4477 6 12 6H17C17.5523 6 18 6.44772 18 7C18 7.55228 17.5523 8 17 8H12C11.4477 8 11 7.55228 11 7ZM7 8.75C7.9665 8.75 8.75 7.9665 8.75 7C8.75 6.0335 7.9665 5.25 7 5.25C6.0335 5.25 5.25 6.0335 5.25 7C5.25 7.9665 6.0335 8.75 7 8.75Z',
    fill: '#FFFFFF',
    'stroke-width': '0.5'
  })
]);

const helpSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('path', {
    d: 'M12 17H12.01M12 14C12.8906 12.0938 15 12.2344 15 10C15 8.5 14 7 12 7C10.4521 7 9.50325 7.89844 9.15332 9M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z',
    stroke: '#ffa726',
    'stroke-width': '2',
    'stroke-linecap': 'round',
    'stroke-linejoin': 'round'
  })
]);

const post2SVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 210.439 210.439',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg',
  'xmlns:xlink': 'http://www.w3.org/1999/xlink',
  'xml:space': 'preserve'
}, [
  h('g', {}, [
    h('path', {
      d: 'M197.762,169.999L177.324,5.262c-0.408-3.288-3.398-5.625-6.693-5.215L5.893,20.484c-3.005,0.373-5.261,2.927-5.261,5.955 v166c0,3.314,2.686,6,6,6h166c3.313,0,6-2.686,6-6v-14.021l13.915-1.727c1.579-0.196,3.016-1.011,3.994-2.266 C197.518,173.17,197.958,171.578,197.762,169.999z M20.436,186.439h-7.805V123.53L20.436,186.439z M13.882,40.181 c-0.186,0.023-0.367,0.057-0.546,0.096l-1.183-9.536l154.915-19.22l1.18,9.509L13.882,40.181z M166.632,186.439h-52.65l52.65-6.532 V186.439z M31.372,185.656L14.805,52.124c0.184-0.006,0.368-0.011,0.554-0.034l154.365-19.151l16.563,133.497L31.372,185.656z',
      fill: '#FFFFFF', // Changement de la couleur en blanc
    })
  ])
]);

const paletteSVG = h('svg', {
  width: '800px',
  height: '800px',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
}, [
  h('path', {
    d: 'M5.57489 2.07403C5.83474 2.19892 6 2.4617 6 2.75001C6 3.57985 6.31211 4.05763 6.70313 4.63948L6.73156 4.68175C7.0641 5.17579 7.5 5.8234 7.5 6.75001C7.5 7.69552 7.02282 8.52959 6.29615 9.02452C6.48733 9.1848 6.65672 9.38248 6.80225 9.61803C7.27801 10.388 7.5 11.5645 7.5 13.2549C7.5 14.967 7.27003 17.023 6.89541 18.6644C6.70914 19.4806 6.47843 20.2335 6.20272 20.7994C6.06598 21.08 5.89948 21.3541 5.69217 21.5685C5.48714 21.7804 5.17035 22.0049 4.75 22.0049C4.32965 22.0049 4.01286 21.7804 3.80783 21.5685C3.60052 21.3541 3.43402 21.08 3.29728 20.7994C3.02157 20.2335 2.79086 19.4806 2.60459 18.6644C2.22997 17.023 2 14.967 2 13.2549C2 11.5645 2.22199 10.388 2.69775 9.61803C2.84328 9.38248 3.01267 9.1848 3.20385 9.02452C2.47718 8.52959 2 7.69552 2 6.75001C2 6.38181 2.00034 5.74889 2.38341 4.93168C2.75829 4.13192 3.47066 3.21301 4.78148 2.16436C5.00661 1.98425 5.31504 1.94914 5.57489 2.07403ZM3.5 6.74875V6.75001C3.5 7.44036 4.05964 8.00001 4.75 8.00001C5.44036 8.00001 6 7.44036 6 6.75001C6 6.31097 5.81518 6.00743 5.45814 5.47615L5.44592 5.45796C5.21705 5.11747 4.94673 4.71532 4.75381 4.19756C4.21053 4.74999 3.9105 5.208 3.74159 5.56833C3.5 6.08374 3.5 6.4505 3.5 6.74875ZM3.97383 10.4065C3.72572 10.808 3.5 11.6315 3.5 13.2549C3.5 14.8565 3.71774 16.8005 4.06698 18.3306C4.24264 19.1003 4.44289 19.726 4.64574 20.1424C4.68308 20.219 4.71806 20.2834 4.75 20.3369C4.78194 20.2834 4.81692 20.219 4.85426 20.1424C5.05711 19.726 5.25736 19.1003 5.43302 18.3306C5.78226 16.8005 6 14.8565 6 13.2549C6 11.6315 5.77428 10.808 5.52617 10.4065C5.41327 10.2237 5.30119 10.1372 5.20105 10.0886C5.09322 10.0363 4.95068 10.0049 4.75 10.0049C4.54932 10.0049 4.40678 10.0363 4.29895 10.0886C4.19881 10.1372 4.08673 10.2237 3.97383 10.4065Z',
    fill: '#FFFFFF', // Changement de la couleur en blanc
  }),
  h('path', {
    d: 'M9.99994 14.917C9.46162 14.8267 8.94761 14.6647 8.46806 14.4412C8.48904 14.0349 8.49994 13.637 8.49994 13.2549C8.49994 13.0791 8.49768 12.9066 8.49298 12.7376C8.94409 13.0407 9.4531 13.2644 9.99994 13.3885V10.5C9.99994 9.67157 10.6715 9 11.4999 9H15.4999C15.4999 6.51472 13.4852 4.5 10.9999 4.5C9.97153 4.5 9.0237 4.84498 8.26586 5.42552C8.06633 4.8731 7.78116 4.44995 7.58275 4.15554L7.54248 4.09572C8.51976 3.40549 9.7125 3 10.9999 3C14.3136 3 16.9999 5.68629 16.9999 9H20.4999C21.3284 9 21.9999 9.67157 21.9999 10.5V19.5C21.9999 20.3284 21.3284 21 20.4999 21H11.4999C10.6715 21 9.99994 20.3284 9.99994 19.5V14.917ZM11.4999 14.9795V19.5H20.4999V10.5H16.8109C16.185 12.932 14.0726 14.7672 11.4999 14.9795ZM15.2439 10.5H11.4999V13.4725C13.239 13.2803 14.6794 12.097 15.2439 10.5Z',
    fill: '#FFFFFF', // Changement de la couleur en blanc
  })
]);

CMS.registerIcon('homePage', () => homeSVG);
CMS.registerIcon('menuPage', () => menuSVG);
CMS.registerIcon('settingsPage', () => settingsSVG);
CMS.registerIcon('postPage', () => post2SVG);
CMS.registerIcon('helpPage', () => helpSVG);
CMS.registerIcon('palette', () => paletteSVG);

function htmlToH(text) {
  const parser = new DOMParser();
  const doc = parser.parseFromString(text, 'text/html');
  const element = doc.body.firstChild;

  function parseNode(node) {
      if (node.nodeType === Node.TEXT_NODE) {
          return node.textContent;
      } else if (node.nodeName == 'IMG') {
          const props = {};
          // Récupérer les attributs HTML en tant que propriétés
          for (const attr of node.attributes) {
            props[attr.name] = attr.value;
          }
          return h('img', props);
      } else if (node.nodeType === Node.ELEMENT_NODE) {
          const props = {};
          // Récupérer les attributs HTML en tant que propriétés
          for (const attr of node.attributes) {
              const parties = attr.value.trim().split(";").filter(Boolean);
              for (const part of parties) {
                if (props[attr.name] == undefined) {
                  props[attr.name] = {};
                }
                const kv = part.split(":");
                const k = kv[0].trim();
                const v = kv[1].trim();
                props[attr.name][k] = v;
              } 
          }
          // Ajouter les styles CSS aux propriétés
          if (props.style == undefined) {
            props.style = {};
          }
          const children = Array.from(node.childNodes).map(parseNode);
          return h(node.tagName.toLowerCase(), props, ...children);
      }
  }

  return parseNode(element);
}

const TutorialPage = () => {
  const htmlTutorial = `
<pre>
<pre style="font-size: 40px;">
Bienvenue au tutoriel d’utilisation de Palette !
</pre>
Palette vous permet de modifier et personnaliser facilement vos sites. Voici les différentes catégories disponibles dans l’interface :

<img src="tutorial/liste.png" alt="Différentes catégories disponibles">
        <strong>  Page principale :</strong> Vous pouvez choisir le contenu de la page d’accueil de votre site
        <strong>  Pages de sites :</strong> Vous pouvez créer et éditer des pages supplémentaires à ajouter à votre site
        <strong>  Configuration :</strong> Vous pouvez personnaliser l’aspect global (thème) du site ainsi que les menus
        <strong>  Tutoriel :</strong> Vous y êtes (;
        <strong>  Media :</strong> Vous pouvez ajouter, supprimer et naviguer dans les images que vous pouvez ensuite intégrer dans l’édition de contenu de vos pages ainsi que dans les configurations.

<hr>

<pre style="font-size: 30px; text-decoration: underline">Edition de page :</pre>

Lorsque vous éditez une page vous pouvez éditer :

<img src="tutorial/editeur.png" alt="Edition">
        <strong>  Titre :</strong> Nom de la page
        <strong>  Description :</strong> Courte description de la page
        <strong>  Image de couverture :</strong> Image de bannière de la page
        <strong>  Corps :</strong> Vous pouvez renseigner du texte, images, liens, citations… pour le contenu de votre page
    
<strong style="color: yellow; font-size: 25px;">N’oubliez pas de cliquer sur le bouton ‘Publier’ en haut à droite lorsque vous avez terminé</strong>

<hr>

<pre style="font-size: 30px; text-decoration: underline">Configuration :</pre>

Vous pouvez personnaliser le thème:

<img src="tutorial/theme.png" alt="Gestion du theme">
        <strong>Logo du site :</strong>Le logo du site dans le coin supérieur gauche du site
        <strong>Favicon :</strong>Le logo du site (favicon) qui apparaitra dans les onglets du navigateur
        <strong>Couleur du fond :</strong>La couleur de fond du site
        <strong>Réseaux sociaux :</strong>Des liens vers des réseaux sociaux

Vous pouvez personnaliser les menus:

<img src="tutorial/menus.png" alt="Gestion des menus">
Les menus permettent de naviguer entre les différentes pages que vous avez créé. Pour créer un lien vers une page que vous avez créé, ajouter une Section :
        <strong>Nom dans le menu :</strong> Le nom que vous voulez qui soit affiché
        <strong>Page référencée :</strong> Indiquez /pages/&lt;le nom de la page créée&gt; (par exemple : /pages/contactez-nous) [Pour la page d'accueil, renseignez uniquement / (comme sur l'image précédente)]
        <strong>Poids :</strong> Est la place que prend le nom du menu dans la page (par défaut vous pouvez le laisser à 1)

<strong style="color: yellow; font-size: 25px;">N’oubliez pas de cliquer sur le bouton ‘Publier’ en haut à droite lorsque vous avez terminé</strong>
</pre>`
  return htmlToH(htmlTutorial);
};

CMS.registerAdditionalLink({
  id: 'tutorial',
  title: 'Tutoriel',
  data: TutorialPage,
  options: {
    icon: 'helpPage',
  },
});

CMS.init();